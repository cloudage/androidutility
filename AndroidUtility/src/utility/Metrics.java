package utility;

import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;


/**
 * 此工具用于像素与dp之间的转换
 * @author cloudage
 *
 */
public class Metrics {
	
	/**
	 * 将dp转换为像素
	 * @param ctx 传入当前activity
	 * @param dp 要转换的dp值
	 * @return 当前系统对应dp的像素值
	 */
	public static int dpToPixel(Context ctx,float dp){
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();    	
		return (int) (metrics.density * dp + 0.5f);
	}
	
	/**
	 * 将像素转换为dp
	 * @param ctx 传入当前activity
	 * @param pixel 要转换的像素值
	 * @return 当前系统对应像素值的dp值
	 */
	public static int pixelToDp(Context ctx,float pixel){
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();    	
		return (int)((pixel-0.5f)/metrics.density);
	}
	
	/**
	 * 获取屏幕尺寸
	 * @param ctx 传入当前activity
	 * @return 当前屏幕的尺寸，单位为像素
	 */
	public static Rect screenSizePixel(Context ctx){
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		return new Rect(0, 0, metrics.widthPixels, metrics.heightPixels);
	}
}
