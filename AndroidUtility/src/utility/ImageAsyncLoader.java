package utility;

import java.io.File;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import filesystem.LocalStorage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


/**
 * 此控件用于异步加载图片
 * @author cloudage
 *
 */
public class ImageAsyncLoader extends RelativeLayout {
	
	private static File genCacheFile(String url,Context context){
		String cacheFileName = Digest.md5(url);
		File cacheFile = new File(context.getCacheDir().getPath()+'/'+cacheFileName);
		return cacheFile;
	}

	private static byte[] downloadFromUrl(String url){
		try{
			DefaultHttpClient dhc = new DefaultHttpClient();					
			HttpUriRequest req = new HttpGet(url);					
			HttpResponse response = dhc.execute(req);
			return EntityUtils.toByteArray(response.getEntity());

		}catch(Exception ex){
			return null;
		}
	}

	/**
	 * 预载图片
	 * @param url 图片的完整地址
	 * @param context 当前activity
	 * @return 若预载成功则返回 true
	 */
	public static boolean prepareUrl(String url,Context context){
		File cacheFile = genCacheFile(url, context);
		if(cacheFile.exists())return true;
		
		byte[] result = downloadFromUrl(url);
		
		if(result!=null)
			LocalStorage.write(cacheFile.toString(), result);

		return result!=null;
	}
	
	/**
	 * 获取已缓存的图片
	 * @param url 图片完整地址
	 * @param context 当前activity
	 * @return 若成功获取，则返回该图片的本地缓存副本，否则返回 null
	 */
	public static Bitmap loadCachedBitmap(String url,Context context){
		prepareUrl(url, context);
		File cacheFile = genCacheFile(url, context);		
		if(cacheFile.exists()){
			byte[] data = LocalStorage.read(cacheFile.toString());
			try{
				return BitmapFactory.decodeByteArray(data, 0, data.length);
			}catch(Exception ex){
				LocalStorage.delete(cacheFile.toString());
			}			
		}
		
		return null;
	}
	
	public ImageAsyncLoader(Context context) {
		super(context);
		genSubViews();
	}

	public ImageAsyncLoader(Context context, AttributeSet attrs) {
		super(context, attrs);
		genSubViews();
	}

	private ImageView imageView;
	private ProgressBar progressBar;

	/**
	 * @return 用于显示图片的ImageView控件
	 */
	public ImageView getImageView(){
		return imageView;
	}

	private void genSubViews(){		
		imageView = new ImageView(getContext());
		imageView.setScaleType(ScaleType.FIT_XY);

		progressBar = new ProgressBar(getContext(),null,android.R.attr.progressBarStyleSmall);		
		progressBar.setMax(100);

		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(CENTER_IN_PARENT);
		this.addView(progressBar,lp);

		lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		this.addView(imageView,lp);

		imageView.setVisibility(View.INVISIBLE);

		this.setBackgroundColor(0x333333);
	}

	/**
	 * 此接口用于监听图片加载事件
	 * @author cloudage
	 *
	 */
	public interface OnLoadingResultListener{
		/**
		 * 当图片加载完成时，将调用此方法
		 * @param result 图片的原始文件流
		 */
		void onLoadingResult(byte[] result);
	}

	private class LoadingWorker extends AsyncTask<Void, Integer, Void>
	{
		private String url;
		private OnLoadingResultListener onLoadingResult;
		private byte[] result;

		public LoadingWorker(String url,OnLoadingResultListener onResult){
			this.url = url;
			this.onLoadingResult = onResult;
		}

		@Override
		protected Void doInBackground(Void... params) {
			result = downloadFromUrl(url);

			return null;
		}

		protected void onPreExecute() {
			currentWorker=this;
		};

		protected void onPostExecute(Void r) {			
			if(currentWorker!=this)return;
			currentWorker=null;
			onLoadingResult.onLoadingResult(result);
		};
	};

	private LoadingWorker currentWorker;

	private boolean onLoadResult(byte[] data){
		try{
			Bitmap result = BitmapFactory.decodeByteArray(data, 0, data.length);
			imageView.setImageBitmap(result);
			imageView.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.INVISIBLE);
			return true;
		}catch(Exception ex){
			return false;
		}
	}

	/**
	 * 开始异步加载一张图片
	 * @param url 图片的完整地址
	 */
	public void loadUrl(String url){
		imageView.setImageBitmap(null);
		imageView.setVisibility(View.INVISIBLE);
		progressBar.setVisibility(View.VISIBLE);

		final File cacheFile = genCacheFile(url, getContext());

		if(cacheFile.exists()){
			byte[] data = LocalStorage.read(cacheFile.toString());

			if(onLoadResult(data))
				return;
			else
				LocalStorage.delete(cacheFile.toString());
		}

		LoadingWorker loadingWorker = new LoadingWorker(
				url,
				new OnLoadingResultListener() {
					public void onLoadingResult(byte[] result) {
						if(onLoadResult(result))
							LocalStorage.write(cacheFile.toString(), result);						
					}
				});
		loadingWorker.execute();
	}
}
