package utility;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/** 
 * 此工具中包含键盘相关的操作
 * @author cloudage
 *
 */
public class Keyboard {
	
	
	/**
	 * 隐藏软键盘
	 * @param context 传入当前activity
	 */
	public static void hideKeyboard(Context context){
		try{
			Activity act = (Activity)context;
			InputMethodManager ims = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			if(ims==null)return;
			ims.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
