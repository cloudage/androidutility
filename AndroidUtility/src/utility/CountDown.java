package utility;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 倒计时工具
 * @author cloudage
 *
 */
public class CountDown{
	private int delay;	
	private int start;
	private int step;
	private int current;

	private boolean running = false;	
	private Timer timer = null;

	/**
	 * @return 当前的倒计数
	 */
	public int getCurrent(){
		return current;
	}

	public static interface Listener{
		/**
		 * 倒计时发生时会调用此方法
		 * @param cd 当前倒计数
		 * @return 返回是否应该继续倒数
		 */
		boolean counting(CountDown cd);
	}

	private Listener listener;

	/**
	 * @param delay 倒数间隔时间
	 * @param start 倒数起始值
	 * @param step 倒数步长。每次倒数发生时，将从当前倒计数中减去步长
	 * @param listener 事件侦听器
	 */
	public CountDown(int delay, int start, int step, Listener listener){
		this.delay = delay;
		this.start = start;
		this.current = start;
		this.step = step;
		this.listener = listener;
	}

	/**
	 * 开始倒计时
	 */
	public void start() {
		if(running){
			timer.cancel();
		}

		running = true;
		timer = new Timer();
		current = start;

		TimerTask task = new TimerTask() {			
			@Override
			public void run() {
				if(!running) return;

				current -= step;
				if(!listener.counting(CountDown.this)){					
					stop();
				}
			}
		};

		timer.schedule(task, 0, delay);
	}

	/**
	 * 停止倒计时
	 */
	public void stop() {
		if(!running) return;
		running = false;
		timer.cancel();
		timer = null;
	}
}
