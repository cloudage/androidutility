package utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 此处提供采样算法工具
 * @author cloudage
 *
 */
public class Digest {
	/**
	 * MD5 哈西算法
	 * @param input 输入字符串
	 * @return 如果系统不支持MD5算法，则返回null，否则返回MD5结果。
	 */
	public static String md5(String input){		
		try {
			StringBuilder sb = new StringBuilder();
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(input.getBytes());
			byte[] md5 = algorithm.digest();
			String tmp = "";
			for (int i = 0; i < md5.length; i++) {
				tmp = (Integer.toHexString(0xFF & md5[i]));
				if (tmp.length() == 1) {
					sb.append(0);
				}
				
				sb.append(tmp);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			return null;
		}
	}
}
