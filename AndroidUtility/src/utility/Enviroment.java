package utility;

import java.util.UUID;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * 此处用于存放全局环境变量
 * @author cloudage
 *
 */
public class Enviroment {

	/**
	 * 关于地理位置的变量
	 * @author cloudage
	 *
	 */
	public static class Location{
		/**
		 * gps定位方式是否可用
		 */
		public static boolean gpsAvilable =false;
		/**
		 * wifi定位方式是否可用
		 */
		public static boolean wifiAvilable =false;
		/**
		 * 纬度
		 */
		public static double lat =0;
		/**
		 * 经度
		 */
		public static double lon =0;
		/**
		 * 海拔
		 */
		public static double alt =0;
	}

	public static class Device{
		public static String UUID;		
	}
}
