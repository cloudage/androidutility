package utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HtmlContentView extends LinearLayout {
	
	public HtmlContentView(Context context) {
		super(context);
		setOrientation(LinearLayout.VERTICAL);
	}
	
	public HtmlContentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(LinearLayout.VERTICAL);
	}

	static Pattern pFindImg = Pattern.compile("<\\s*img\\s*[^>]*>");

	private String urlRoot;
	
	public void loadHtml(String source, String urlRoot){
		removeAllViews();

		this.urlRoot = urlRoot;
		
		Matcher matcher = pFindImg.matcher(source);
		int lastEnd = 0;		
		while(matcher.find()){
			String txt = source.substring(lastEnd, matcher.start());
			String img = matcher.group();

			if(txt!=null){

				addView(makeTextView(txt));
				addView(makeGapView());
			}

			if(img!=null){
				View v = makeImageLoader(img);
				if(v!=null){
					addView(v);
					addView(makeGapView());
				}
			}

			lastEnd = matcher.end();
		}

		String finaltxt = source.substring(lastEnd);
		if(finaltxt!=null){
			addView(makeTextView(finaltxt));
		}		
	}
	
	static Pattern pFindSrc = Pattern.compile("\\s+src\\s*=\"[^\"]*\"");
	private ImageAsyncLoader makeImageLoader(String imgHtmlTag){
		Matcher matcher = pFindSrc.matcher(imgHtmlTag);
		if(matcher.find()){
			String src = matcher.group();
			src = src.substring(src.indexOf('"')+1, src.lastIndexOf('"'));
			ImageAsyncLoader ial = new ImageAsyncLoader(getContext());
			if(!src.startsWith("http")){
				src = urlRoot + src;
			}
			ial.loadUrl(src);
			ial.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			return ial;
		}else{
			return null;
		}
	}
	private TextView makeTextView(String htmlContent){
		TextView txtV = new TextView(getContext());
		txtV.setTextColor(0xff000000);
		txtV.setLineSpacing(Metrics.dpToPixel(getContext(), 4),1);
		txtV.setText(Html.fromHtml(htmlContent));
		txtV.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		return txtV;
	}

	private View makeGapView(){
		View v = new View(getContext());
		v.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, Metrics.dpToPixel(getContext(), 1)));
		return v;
	}
}
