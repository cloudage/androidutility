package utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Network {
	public static boolean isConnected(Context ctx){
		try { 
			ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE); 
			if (connectivity != null) { 
				NetworkInfo info = connectivity.getActiveNetworkInfo(); 

				if (info != null&& info.isConnected()) { 
					if (info.getState() == NetworkInfo.State.CONNECTED) { 
						return true; 
					} 
				} 
			} 
		} catch (Exception e) {  
			e.printStackTrace();
		}

		return false;
	} 
}
