package utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * 此工具用于字节流相关的操作
 * @author cloudage
 *
 */
public class ByteArray {
	/**
	 * 将字节流按照UTF-8编码转换为字符串
	 * @param byteArray 字节流
	 * @return 转换成功则返回字符串，否则返回空字符串
	 */
	public static String byteArrayToString(byte[] byteArray) {
		if(byteArray==null)return "";
		try {
			return new String(byteArray, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * 将对象序列化为字节流
	 * @param obj
	 * @return 若成功则返回字节流，否则返回 null
	 */
	public static byte[] objectToByteArray(Serializable obj){
		try{
			ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
			ObjectOutputStream oos = new ObjectOutputStream(bos); 
			oos.writeObject(obj);
			oos.flush(); 
			oos.close(); 
			bos.close();
			byte [] data = bos.toByteArray();	
			return data;
		}
		catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将字节流反序列化为对象
	 * @param bytearray
	 * @return 若成功则返回对象，否则返回 null
	 */
	public static Serializable byteArrayToObject(byte[] bytearray){
		try {
			ByteArrayInputStream ios = new ByteArrayInputStream(bytearray);
			ObjectInputStream ois = new ObjectInputStream(ios);
			Serializable obj = (Serializable)ois.readObject();
			ois.close();
			ios.close();
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 创建对象副本
	 * @param orginal 原对象
	 * @return 副本对象
	 */
	public static Serializable duplicate(Serializable orginal){
		return byteArrayToObject(objectToByteArray(orginal));
	}
}
