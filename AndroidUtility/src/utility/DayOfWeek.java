package utility;

import java.util.Calendar;
import java.util.Date;

/**
 * 此工具将dayOfWeek转换为中文
 * @author cloudage
 *
 */
public class DayOfWeek {
	/**
	 * @param dayOfWeek
	 * @return 根据dayOfWeek，返回 “星期日” “星期一” “星期二” 等等中文
	 */
	public static String getName(int dayOfWeek){
		switch(dayOfWeek){
		case Calendar.SUNDAY: return "星期日";
		case Calendar.MONDAY: return "星期一";
		case Calendar.TUESDAY: return "星期二";
		case Calendar.WEDNESDAY: return "星期三";
		case Calendar.THURSDAY: return "星期四";
		case Calendar.FRIDAY: return "星期五";
		case Calendar.SATURDAY: return "星期六";
		default:return null;
		}
	}
	
	public static String getName(Calendar cale){
		return getName(cale.get(Calendar.DAY_OF_WEEK));
	}
	
	public static String getName(Date date){
		Calendar cale = Calendar.getInstance();
		cale.setTime(date);
		return getName(cale);
	}
}
