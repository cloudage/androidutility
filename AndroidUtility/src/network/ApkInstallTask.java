package network;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;

public class ApkInstallTask extends AsyncTask<Void, Integer, Void>  {
	private Context context;
	private String url;
	private boolean force;
	
	private String filepath;
	
	private ProgressDialog progress;

	private void alertLimitedBySecurePermission(){
		AlertDialog ad = new AlertDialog.Builder(context)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("权限受限")
		.setCancelable(false)
		.setMessage("需要在设置中开启“安装非电子市场提供的应用程序”")
		.setNeutralButton("现在设置", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intent = new Intent();      
				intent.setAction(Settings.ACTION_APPLICATION_SETTINGS);      
				context.startActivity(intent);     
				arg0.dismiss();
			}
		})
		.setNegativeButton("暂不设置", new DialogInterface.OnClickListener() {				
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if(force){
					android.os.Process.killProcess(android.os.Process.myPid()); 
					System.exit(0); 
				}
			}
		})
		.create();

		ad.show();
	}
	
	private void alertDownloadFailed(){
		AlertDialog alert = new AlertDialog.Builder(context)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("下载失败")
		.setMessage("无法完成下载，请稍后再试")
		.setCancelable(false)
		.setNeutralButton("确定", new DialogInterface.OnClickListener() {				
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
				if(force){
					android.os.Process.killProcess(android.os.Process.myPid()); 
					System.exit(0); 
				}
			}
		})
		.create();
		alert.show();
	}

	private void alertChangePermissionFailed() {
		new AlertDialog.Builder(context)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("安装失败")
		.setMessage("修改权限失败，无法启动安装过程")
		.setCancelable(false)
		.setNeutralButton("确定", new DialogInterface.OnClickListener() {				
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
				if(force){
					android.os.Process.killProcess(android.os.Process.myPid()); 
					System.exit(0); 
				}
			}
		})
		.show();
	}
	
	public ApkInstallTask(Context context,String url,boolean force){
		this.context = context;
		this.url = url;
		this.force = force;
		this.filepath = new File(context.getCacheDir(),"update.apk").getAbsolutePath();
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		try {
			int canInstallNonMarketApps = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS);
			
			if (canInstallNonMarketApps==0){
				alertLimitedBySecurePermission();
				this.cancel(true);
				return;
			}
			
			File file = new File(filepath);
			if(file.exists())
				file.delete();
			
			progress = new ProgressDialog(context);
			progress.setTitle("下载中，请稍候");
			progress.setMax(100);
			progress.setIndeterminate(false);
			progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progress.show();
		} catch (Exception e) {
			e.printStackTrace();
			filepath=null;
			this.cancel(true);
		}		
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		progress.dismiss();
		
		if(filepath==null || (!new File(filepath).exists())){
			alertDownloadFailed();
		}else{
			String cmd = "chmod 755 "+filepath;

			try {
				Runtime.getRuntime().exec(cmd);
			} catch (Exception e) {
				e.printStackTrace();
				alertChangePermissionFailed();
				return;
			}   

			Intent intent = new Intent();
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(filepath)),	"application/vnd.android.package-archive");
			context.startActivity(intent);

			android.os.Process.killProcess(android.os.Process.myPid()); 
			System.exit(0); 
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		progress.setProgress(values[0]);		
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		int count;
		try {
			URL downurl = new URL(url);
			URLConnection conexion = downurl.openConnection();
			conexion.connect();
			// this will be useful so that you can show a tipical 0-100% progress bar
			int lenghtOfFile = conexion.getContentLength();

			// download the file
			InputStream input = new BufferedInputStream(downurl.openStream());
			OutputStream output = new FileOutputStream(filepath);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				publishProgress((int)(total*100/lenghtOfFile));
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();
		} catch (Exception e) {
			filepath=null;
			e.printStackTrace();					
		}
		
		return null;
	}
}
