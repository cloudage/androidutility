package network;

import java.util.HashMap;
import java.util.Map;

public class HttpQuery{
	public enum HttpQueryMethod{
		Get,Post
	};
	
	public Map<String, Object> params = new HashMap<String, Object>();
	public String url;
	public HttpQueryMethod method = HttpQueryMethod.Get;
	public HttpQueryResultReceiver callback;
	
	public void start() {
		HttpQueryQueue.addQuery(this);
	}
}
