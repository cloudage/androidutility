package network;

import org.apache.http.StatusLine;

public interface HttpQueryResultReceiver {
	void onResult(byte[] responseBody,StatusLine status, Exception error);
}
