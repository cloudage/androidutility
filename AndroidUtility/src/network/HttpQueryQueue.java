package network;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import network.HttpQuery.HttpQueryMethod;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class HttpQueryQueue {
	private static List<HttpQuery> queue = new LinkedList<HttpQuery>();
	private static Boolean running = false;	
	private static HttpClient httpClient;

	static
	{
		DefaultHttpClient dhc = new DefaultHttpClient();
		dhc.setCookieStore(new BasicCookieStore());
		dhc.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
		dhc.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 5000);
		httpClient=dhc;
	}

	private static Runnable queueExecutor = new Runnable() {

		private void doRequest(HttpQuery q,HttpUriRequest request)
		{
			Log.d("HttpQueue Request", q.method + " " + q.url);
			
			if(q.params!=null) Log.d("HttpQueue params", q.params.toString());
			
			byte[] result = null;
			StatusLine status = null;
			Exception error = null;

			try {				
				HttpResponse response = httpClient.execute(request);

				result=EntityUtils.toByteArray(response.getEntity());

				status=response.getStatusLine();								
			} catch (Exception e) {
				error=e;				
			}

			if(q.callback!=null)
			{
				q.callback.onResult(result, status, error);
			}
			
			if(status!=null){
				Log.d("HttpQueue Request", "Status "+status.getStatusCode());
			}else{
				Log.d("HttpQueue Request", "Status is null");
			}
			
			if(error!=null){
				Log.d("HttpQueue Error", error.getLocalizedMessage());
			}
		}

		private void doGet(HttpQuery q)
		{
			String url = q.url;
			if(q.params!=null && q.params.size()>0)
			{	
				StringBuilder sb = new StringBuilder();
				Iterator<String> i = q.params.keySet().iterator();
				while (i.hasNext()) {
					sb.append('&');
					String key = i.next();
					Object objval = q.params.get(key);
					if(objval==null) continue;
					String value = objval.toString();
					sb.append(key);
					sb.append('=');
					sb.append(value);					
				}

				Boolean queryAlreadyBegins = false;
				for(int ic=url.length()-1;ic>=0;ic--){
					if(url.charAt(ic)=='?'){
						queryAlreadyBegins = true;
						break;
					}
				}
				url+= queryAlreadyBegins ? sb.toString() : sb.toString().replaceFirst("&", "?");
			}

			HttpGet request = new HttpGet(url);

			doRequest(q,request);
		}

		private void doPost(HttpQuery q)
		{
			HttpPost request = new HttpPost(q.url);
			if(q.params!=null)
			{
				MultipartEntity multipart = new MultipartEntity();
				Iterator<String> i = q.params.keySet().iterator();
				while(i.hasNext())
				{
					String key = i.next();
					Object value = q.params.get(key);
					if(value==null) continue;
					
					ContentBody body = null;
					if(value.getClass().equals(byte[].class))
					{
						body = new ByteArrayBody((byte[]) value, key);
					}
					else
					{
						try {
							body = new StringBody(value.toString(), Charset.forName("UTF-8"));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					multipart.addPart(key, body);
				}
				request.setEntity(multipart);
			}

			doRequest(q, request);
		}

		public void run() {
			while(true)
			{
				HttpQuery q = null;
				synchronized (queue) {
					if(queue.size()>0)
					{
						q = queue.remove(0);
					}
					else {
						running=false;
						return;
					}
				}

				switch (q.method) {
				case Post:
					doPost(q);
					break;
				default:
					doGet(q);
					break;
				}
			}
		}
	};

	private HttpQueryQueue(){}

	private static void run()
	{
		synchronized (queue) {
			if(running)return;

			running=true;
		}

		new Thread(queueExecutor).start();		
	}

	public static void addQuery(HttpQuery query)
	{
		synchronized (queue) {
			queue.add(query);			
		}		

		Log.d("HttpQueue Add", query.url);
		
		run();
	}
}
