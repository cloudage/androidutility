package filesystem;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import android.os.Environment;

public class LocalStorage {
	private static void delete(File file){
		if (file.isDirectory()) {
			try {
				for (File child:file.listFiles()) {
					if (child.isDirectory()) 
						delete(child);			
					child.delete();
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		file.delete();
	}
	
	public static void delete(String fullpath){
		File f = new File(fullpath); 
		if(f.exists())delete(f);
	}

	public static boolean write(String fullpath,byte[] data){
		delete(fullpath);
		try{			
			FileOutputStream fos = new FileOutputStream(fullpath, false);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bos.write(data);
			bos.flush();
			bos.close();
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}	
	}

	public static byte[] read(String fullpath){
		try{
			FileInputStream fis = new FileInputStream(fullpath);
			BufferedInputStream bis = new BufferedInputStream(fis);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			byte[] temp = new byte[1024];      
			int size = 0;      
			while ((size = bis.read(temp)) != -1) {      
				baos.write(temp, 0, size);      
			}  
			bis.close();

			return baos.toByteArray();
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	public static Object readObject(String path){
		try {
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();
			ois.close();		
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean writeObject(String path,Serializable object){
		delete(path);
		try {
			FileOutputStream fos = new FileOutputStream(path,false);
			ObjectOutputStream oos = new ObjectOutputStream(fos);		
			oos.writeObject(object);
			oos.flush();
			oos.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean exsists(String path){
		return new File(path).exists();
	}
	
	public static void rename(String oldname, String newname) {
		boolean result = new File(oldname).renameTo(new File(newname));
		assert(result);
	}
	
	public static boolean isExternalStorageAvailable(boolean requireWriteAccess){
		String state = Environment.getExternalStorageState();  
		  
	    if (Environment.MEDIA_MOUNTED.equals(state)) {  
	        return true;  
	    } else if (!requireWriteAccess  
	            && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {  
	        return true;  
	    }  
	    return false;  
	}
	
	public static boolean isExternalStorageAvailable(){
		return isExternalStorageAvailable(true);
	}
}
